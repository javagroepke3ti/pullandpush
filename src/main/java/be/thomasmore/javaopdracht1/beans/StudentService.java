/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.javaopdracht1.beans;

import be.thomasmore.javaopdracht1.student.ExcelReaderImpl;
import be.thomasmore.javaopdracht1.studentEntity.StudentEntity;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

/**
 *
 * @author Thomas
 */
@Stateless
@Path("student")
public class StudentService{
    
    @PersistenceContext(unitName = "be.thomasmore_JavaOpdracht1_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    //Methode om studenten uit een Excel-file te lezen en in een List te zetten
    public List<StudentEntity> readStudentsFromFile(FileInputStream file) throws FileNotFoundException {
        List<StudentEntity> studenten = new ArrayList<StudentEntity>();
        
        ExcelReaderImpl instance = new ExcelReaderImpl();
        studenten = instance.readStudents(file);
        return studenten;
    }
    
    //Methode om een lijst van studenten op te slaan in de db
    public List<StudentEntity> saveStudents(List<StudentEntity> studenten) {
        for (StudentEntity student : studenten) {
            em.persist(student);
        }
        return studenten;
    }
    
    //Methode om studenten uit een Excel-file te lezen en in een List te zetten en tévens op te slaan in de db
    @POST
    @Path("upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public List<StudentEntity> readStudentsFromFileAndSave(@FormDataParam("file") FileInputStream file) throws FileNotFoundException {
        List<StudentEntity> studenten = saveStudents(readStudentsFromFile(file));
        return studenten;
    }
    
    //Methode om een student uit de db op te halen adv zijn id
    @GET
    @Path("{studentId}")
    public StudentEntity findStudentById(@PathParam("studentId") Long studentId) {
        return em.find(StudentEntity.class, studentId);
    }

    //Methode om alle studenten uit de db op te halen
    @GET
    @Path("allStudents")
    public List<StudentEntity> findAllStudents() {
        List<StudentEntity> studenten = em.createQuery("select s from StudentEntity s order by s.lastName").getResultList();
        return studenten;
    }

}
