/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.javaopdracht1.beans;

import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

/**
 *
 * @author e_for
 */
@ApplicationPath("/rest")
public class MyApplication extends ResourceConfig {
    public MyApplication() {
        super(MultiPartFeature.class);
        packages("be.thomasmore.javaopdracht1.beans", "be.thomasmore.javaopdracht1.studentEntity"); // add all packages containing MultiPartFeature
    }
    
}
