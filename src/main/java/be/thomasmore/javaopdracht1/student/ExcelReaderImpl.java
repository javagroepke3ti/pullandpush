/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.javaopdracht1.student;

import be.thomasmore.javaopdracht1.studentEntity.StudentEntity;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author e_for
 */
public class ExcelReaderImpl implements ExcelReader {

    @Override
    public List<StudentEntity> readStudents(FileInputStream file){
        List<StudentEntity>studenten = new ArrayList<StudentEntity>();
        
        try {
            //Get the workbook instance for XLS file 
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            //Get first sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);
            int teller = 0;
            
            //Iterate through each rows from first sheet
            Iterator<Row> rowIterator = sheet.iterator();
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                StudentEntity student = new StudentEntity();
                
                if (teller > 0)
                {
                    //For each row, iterate through each columns
                    Iterator<Cell> cellIterator = row.cellIterator();
                    while (cellIterator.hasNext()) {
                        Cell cell = cellIterator.next();

                        //Checken in welke kolom de Iterator staat
                        int columnIndex = cell.getColumnIndex();

                        switch (columnIndex) {
                        //Indien in de eerste kolom, moet de waarde in het attribuut lastName
                        case 0:
                            student.setNaam(cell.getStringCellValue());
                            break;
                        //Indien in de tweede kolom, moet de waarde in het attribuut firstName
                        case 1:
                            student.setVoornaam(cell.getStringCellValue());
                            break;
                        //Indien in de derde kolom, moet de waarde in het attribuut email 
                        case 2:
                            student.setEmail(cell.getStringCellValue());
                            break;
                        }
                    }
                    //Het object student, dat nu opgevuld is, toevoegen aan de arrayList
                    studenten.add(student);
                }
                
                teller++;
                
            }
            workbook.close();
            file.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return studenten;
    }    
}
