/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.javaopdracht1.servlets;

import be.thomasmore.javaopdracht1.beans.StudentService;
import be.thomasmore.javaopdracht1.studentEntity.StudentEntity;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Thomas
 **/
@WebServlet(name = "Opdracht1Servlet", urlPatterns = {"/Opdracht1Servlet"})
public class Opdracht1Servlet extends HttpServlet {

    @EJB
    private StudentService studentService;
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        RequestDispatcher rd = null;

        if (request.getParameter("opdracht1") != null)
        {
            try {
                String excel = "../../../../src/main/resources/Opdracht1Test.xlsx";
                FileInputStream file = new FileInputStream(new File(excel));

                List<StudentEntity> studenten = studentService.readStudentsFromFile(file);
                request.setAttribute("studenten", studenten);
                rd = request.getRequestDispatcher("resultaat.jsp");
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {

            }        
        }
        
        rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
