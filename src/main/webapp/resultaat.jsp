<%-- 
    Document   : resultaat
    Created on : 12-okt-2016, 23:04:35
    Author     : e_for
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Resultaat</title>
    </head>
    <body>
        <h1>Resultaat opdrachten:</h1>
        <c:if test="${fn:length(studenten) != 0}">
            <p>Studenten:</p>
            <table border="1">
                <tr>
                    <th>Naam</th>
                    <th>Voornaam</th>
                    <th>Email</th>
                </tr>
                <c:forEach var="student" items="${studenten}">
                    <tr>
                        <td>${student.getNaam()}</td>
                        <td>${student.getVoornaam()}</td>
                        <td>${student.getEmail()}</td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
            <c:if test="${fn:length(studenten) == 0}">
                <p>O jee, 't is mislukt...</p>
            </c:if>    
    
    <p><a href="index.jsp">Terug</a></p>
    </body>
</html>
