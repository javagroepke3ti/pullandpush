<%-- 
    Document   : index
    Created on : 12-okt-2016, 23:03:25
    Author     : e_for
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Opdrachten Java</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <h1>Applicatie-ontwikkeling: Java</h1>
        <p>Opdracht1</p>
        <form method="POST" action="Opdracht1Servlet">
            <input type="submit" value="Opdracht 1" name="opdracht1"/>
        </form>
        
        <p>Opdracht2</p>
        <form method="POST" action="Opdracht2Servlet" enctype="multipart/form-data" >
            File:
            <input type="file" name="file" id="file" /> <br/>
            <input type="submit" value="Upload" name="upload" id="upload" />
        </form>
    </body>
</html>
