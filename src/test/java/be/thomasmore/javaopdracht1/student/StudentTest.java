/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.javaopdracht1.student;

import be.thomasmore.javaopdracht1.studentEntity.StudentEntity;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Thomas
 */
public class StudentTest {
    
    public StudentTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of readStudents method, of class Student.
     */
    @Test
    public void testReadStudents() {
        System.out.println("readStudents");
        ExcelReaderImpl instance = new ExcelReaderImpl();
        try{
            String excel = "./src/test/Opdracht1Test.xlsx";
            FileInputStream file = new FileInputStream(new File(excel));
            List<StudentEntity>studenten = instance.readStudents(file);
            
            assertEquals(4, studenten.size());
            assertEquals("Jonathan", studenten.get(0).getVoornaam());
            assertEquals("Janssens", studenten.get(1).getNaam());
            assertEquals("Jansens", studenten.get(3).getNaam());
            assertEquals("Roy", studenten.get(3).getVoornaam());
            assertEquals("thomasvanrhee.2@gmail.com", studenten.get(2).getEmail());
            
            for (StudentEntity student : studenten)
            {
                System.out.println(student.getVoornaam() + " " + student.getNaam() + " - " + student.getEmail());
            }
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
    
}
